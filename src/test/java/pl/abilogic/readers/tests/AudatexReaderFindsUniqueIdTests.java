package pl.abilogic.readers.tests;

import org.junit.jupiter.api.Test;
import pl.abilogic.readers.EstimateReader;
import pl.abilogic.readers.EstimateReaderFactory;
import pl.abilogic.readers.ReaderException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class AudatexReaderFindsUniqueIdTests {

    @Test
    void findsUniqueEstimateId() throws IOException, ReaderException {
        EstimateReader audatexReader = new EstimateReaderFactory(ToInputStreamConverter.fromString(MockWholeEstimate.WholeEstimate))
                .withCustomStreamExtractor(new MockEstimateStringStreamExtractor());
        //WHEN
        var uniqueEstimateId = audatexReader.findUniqueNr();
        assertFalse(uniqueEstimateId.isBlank());
    }
}
