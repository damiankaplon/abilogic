package pl.abilogic.readers.audatex;

import java.util.List;

interface SectionFinder {

    List<String> findSectionLines();
}
