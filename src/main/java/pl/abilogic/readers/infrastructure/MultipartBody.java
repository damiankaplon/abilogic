package pl.abilogic.readers.infrastructure;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jboss.resteasy.reactive.RestForm;

import javax.ws.rs.FormParam;
import java.io.InputStream;

@Getter
@NoArgsConstructor
public class MultipartBody {
    @RestForm("file")
    public InputStream file;
}
