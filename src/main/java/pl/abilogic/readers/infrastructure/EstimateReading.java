package pl.abilogic.readers.infrastructure;


import pl.abilogic.readers.EstimateReader;
import pl.abilogic.readers.EstimateReaderFactory;
import pl.abilogic.readers.ReaderException;

import javax.enterprise.context.ApplicationScoped;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

@ApplicationScoped
public class EstimateReading {
    public RepairInfo retrieveRepairInfo(EstimateReader estimateReader) throws ReaderException {
        EstimateReader.VehicleInfo vehicleInfo = estimateReader.FindVehicleInfo();
        EstimateReader.Paint paintInfo = estimateReader.FindPaintInfo();
        Set<EstimateReader.Job> labourJobs = estimateReader.findLabourJobs();
        Set<EstimateReader.Job> varnishingJobs = estimateReader.FindVarnishingJobs();
        String uniqueId = estimateReader.findUniqueNr();
        return new RepairInfo(uniqueId, vehicleInfo, paintInfo, labourJobs, varnishingJobs);
    }

    public RepairInfo retrieveRepairInfo(InputStream file) throws IOException, ReaderException {
        EstimateReader estimateReader = new EstimateReaderFactory(file).withPdfStreamExtractor();
        return this.retrieveRepairInfo(estimateReader);
    }

}
