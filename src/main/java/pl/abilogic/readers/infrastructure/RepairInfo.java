package pl.abilogic.readers.infrastructure;

import pl.abilogic.readers.EstimateReader;

import java.util.Set;

public record RepairInfo(String uniqueId,
                         EstimateReader.VehicleInfo vehicleInfo,
                         EstimateReader.Paint paint,
                         Set<EstimateReader.Job> labourJobs,
                         Set<EstimateReader.Job> varnishingJobs){ }
