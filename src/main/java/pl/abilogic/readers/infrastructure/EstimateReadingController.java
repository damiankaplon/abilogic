package pl.abilogic.readers.infrastructure;

import lombok.RequiredArgsConstructor;

import pl.abilogic.readers.ReaderException;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;


@Path("/api/estimate")
@ApplicationScoped
@RequiredArgsConstructor
public class EstimateReadingController {
    private final EstimateReading estimateReading;

    @Path("reader")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public RepairInfo readEstimate(MultipartBody estimate) throws IOException, ReaderException {
        return estimateReading.retrieveRepairInfo(estimate.file);
    }
}
