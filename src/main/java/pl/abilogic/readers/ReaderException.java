package pl.abilogic.readers;

public class ReaderException extends Exception {

    public ReaderException(Throwable cause, String message) {
        super(message, cause);
    }
    public ReaderException(String message) {
        super(message);
    }

}
